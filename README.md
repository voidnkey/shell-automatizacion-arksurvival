# Shell Automatizacion ArkSurvival

Shell para manejar el servidor de ark a traves de esta, el objetivo es automarizar el manejo del servidor con las siguientes funcionalidades

- Actualizar servidor
- Inicializar servidor
- Ver estado del servidor
- Apagar servidor

Actualizar servidor: permite actualizar via steamcmd de forma sencilla.
Inicializar servidor: inicia el servidor dandote a escoger el mapa y el tipo de servidor STEAM / EPIC
ver estado del servidor: te mostrara si esta ONLINE o OFFLINE viendo si existe el proceso corriendo.
Apagar servidor: enviara un kill -15 al proceso para darlo de baja.


